const naiveBayes = require('./naiveBayes').naiveBayes;
const $negative = require('./AdPostsData').adPostsData.split('~~~');
const $positive = require('./GoodPostsData').LemSolaris.split('!');

const positive = [
    'Паттерны работы с реактивными формами в Angular 2: рецепты правильной архитектуры компонентов',
    'Как правильно вставлять SVG изображения – разбор техник работы с векторными изображениями при вёрстке',
    'Подборка пяти примеров работы с CSS фигурами и фильтрами для создания необычных наложений на изображения',
    'Компоненты высшего порядка и функциональные паттерны использования и введение в работу  – новый курс от www.egghead.io',
    'Сравнение паттернов наследования в ES6 – обзор от SitePoint',
    'Как правильно организовать архитектуру React приложения и сделать его расширяемым – туториал на SitePoint',
    '8 советов о том, как стоит и как не стоит писать селекторы в CSS: подборка паттернов и антипаттернов для начинающих'
];

const negative = [
    'Кроссовки “Найк” со скидкой 50 % только с 1 по 5 июля! Доставка — бесплатно',
    'Ищете удобный и вместительный pюкзaк? Тогда "S" создан для Вас! 📌Остерегайтесь дешевых подделок! Скупой платит дважды',
    'Хитовые очки RAY BAN с градиентом, не только шикарно смотрятся, но и защищают ваше зрение!',
    'МЫ нашли группу, которая помогает ставить на ставках.Букмекерские конторы в панике! На данный момент вход бесплатный',
    'Скидка до 70% на брендовые часы 2016-го года. Спешите, осталось небольшое количество ‼Самая большая зимняя РАСПРОДАЖА‼',
    '❗Портмоне Baellerry Leatcher + нож-кредитка в подарок со скидкой 70 % ✅ Воплощение стиля, качества и надежности. 📭 Быстрая доставка без предоплаты ',
    'Нож-кредитка Cardsharp - Оригинал!Отличный подарок как для себя, так и для друзей!Отличное качество и очень острое лезвие СТАРТОВАЯ ЦЕНА: 1 ГРН'
];

// (function test1_tokenization() {
//     console.log('\nTest #1: tokenization');
//
//     const raw = `Посмотреть и заказать➡ https://vk.cc/6enbXO
//                 Настоящее Мужское портмоне Baellery Business - стильный и вместительный бумажник,
//                 сделан из качественной кожи. Это отличный подарок для любого мужчины! 👍
//                 Почему мужчины выбирают кошелек Bailini: ✅ Модный и нестандартный дизайн
//                 ✅ Безупречное выполнение ✅ Прочность и надежность`;
//     console.log(`[${naiveBayes.tokenize(raw).join(', ')}]`);
// })();

// (function test2_teach_classify() {
//
//     console.log('\nTest #2: teach & classify');
//     $negative.forEach(text => naiveBayes.teach(text, 'neg'));
//     $positive.forEach(text => naiveBayes.teach(text, 'pos'));
//
//     console.log(naiveBayes.isNegative($negative[0]));
//     console.log(naiveBayes.isNegative($negative[1]));
//     console.log(naiveBayes.isNegative($positive[0]));
//     console.log(naiveBayes.isNegative($positive[1]));
// })();
// (function test3_teach_classify_unknown() {
//     console.log('\nTest #3: teach & classify unknown');
//     $negative.forEach(text => naiveBayes.teach(text, 'neg'));
//     $positive.forEach(text => naiveBayes.teach(text, 'pos'));
//
//     const test = [
//     'Внимание, в холле можно зарегестрироваться на лекцию по садоводтсву! Участие бесплатное!',//mainly -
//     'Космос и звёзды - что может быть прекрасней?',//mainly +
//     'Не покупайте детям сигареты. Они больше любят мороженое!'//?, but not negative
//     ].forEach(text => console.log(naiveBayes.isNegative(text)));;
// })();
//
// (function test4_teach_classify_unknown() {
//     console.log('\nTest #4: teach range');
//     $negative.forEach(text => naiveBayes.teach(text, 'neg'));
//     $positive.slice(0,(~~($positive.length / 2))).forEach(text => naiveBayes.teach(text, 'pos'));
//
//     const test = [
//         'Внимание, в холле можно зарегестрироваться на лекцию по садоводтсву! Участие бесплатное!',//mainly -
//         'Космос и звёзды - что может быть прекрасней?',//mainly +
//         'Не покупайте детям сигареты. Они больше любят мороженое!'//?, but not negative
//     ].forEach(text => console.log(naiveBayes.isNegative(text)));
// })();

