const fs = require('fs');
const Az = require('az');
const stopWords = require('StopWords');
const ads = require('./AdPostsData').adPostsData.split('~~~');
const good = require('./GoodPostsData').goodPostsData.split('~~~');
const LemSolaris = require('./GoodPostsData').LemSolaris;
const data = good.concat(ads.slice(0, 3));
const Snowball = require('snowball');
const stemmer = new Snowball('Russian');

const adBagOfWords = require('./bagOfWords/AdBagOfWords').adBagOfWords;
const LemSolarisBagOfWords = require('./bagOfWords/LemSolarisBagOfWords').LemSolarisBagOfWords;

let bag = adBagOfWords;
const smoothing = 1;
//************тут бэг оф вордс//

// let tokens = tokenize(ads.toString());
// let tokens = tokenize(LemSolaris);
// bag = createBag(tokens);
// fs.writeFile('./test.txt', JSON.stringify(bag));//JSON.stringify(bag)  or  prepareBagForFile(bag)
// const moreThanTen = bag.filter(b => b.count > 10);
// console.log(moreThanTen, `Len: ${moreThanTen.length}`);
//************//

//************тут я типа нашел среднее значение для рекламы и от него танцую (херня какая-то)//
//good.forEach(text => console.log(getProbabilityOfClass(text, bag)));
// console.log('ads:');
// ads.slice(0, 5).forEach(text =>console.log(getProbabilityOfClass(text, bag), text.substr(0,10)));
// let res = 0;
// const advertiseMidean = 7.13755432959503e-8;
//
// ads.forEach(text =>{
//     let p;
//     if(p = getProbabilityOfClass(text, bag) > advertiseMidean) {
//         console.log(p, text.substr(0,10));
//     }
// });
// console.log(res/ads.length);
//************//

//************тут шото с двумя классами//

let selected = adBagOfWords.find(item => item.word === 'заказать');
console.log(selected.count/(getTotalWordsCount(adBagOfWords)));

const okText = ` прежде всего стремился к тому, чтобы книга легко читалась. Я избегаю неожиданных поворотов; каждый раз, когда в книге упоминается новая концепция, я либо объясняю ее сразу, либо говорю, где буду объяснять.
Основные концепции подкрепляются упражнениями и повторными объяснениями, чтобы вы могли проверить свои предположения и убедиться в том, что не потеряли нить изложения.`;

const badText = `Такого ещё не было!!! 
ЧАСЫ DIESEL BRAVE + часы G-Shock + парфюм Lacoste в подарок! 
Часы от Diesel Brave💥покорившие весь мир, отныне можно купить в России! Стильный мужской аксессуар который несомненно привлечет завистливые взгляды окружающих. 
✔Доставка на почту - Оплата при получении. 
✔ Высокое качество 
✔ Совершенный дизайн 
✔Офигительные подарки 
Заказать по ссылке ➡`;

const badText2 = `ВНИМАНИЕ!!!Скидка 50%!!!
Губная помада «Икона стиля» Giordani Gold💄
Цена 399р😃(обычная цена 850р) 
Успей купить, писать в личку ☺
Роскошные губы, наполненные глубоким, насыщенным цветом, одним движением! 15 роскошных, глубоких оттенков, эксклюзивный дизайн, нежная`;

getProbabilityOfClass(okText);
getProbabilityOfClass(badText);
getProbabilityOfClass(badText2);

// ads.forEach(text => getProbabilityOfClass(text));

//************//

function createBag(words) {
    let bagOfWords = [];
    words.forEach((current, i) => {
        let counter = 1;
        words.forEach((another, j) => {
            if (i === j || !another || !current) {
                return;
            }

            if (current === another) {
                counter++;
                words[j] = null;
            }
        });

        if (current) {
            bagOfWords.push({word: current, count: counter});
        }
    });

    return bagOfWords;
}

function getProbabilityOfClass(text, bagOfWords = adBagOfWords) {
    let negativeBag = adBagOfWords;
    let positiveBag = LemSolarisBagOfWords;
    let count = 0;
    let probability = 0;
    let okProb = 0 + Math.log(1/2);
    let badProb = 0 + Math.log(1/2);
    let adTotalWordsCount = getTotalWordsCount(negativeBag);
    let normTotalWordsCount = getTotalWordsCount(positiveBag);
    let tokens = tokenize('месяц месяц месяц месяц');
    //тут сейчас гвно, надо переписать согласно статье на хабре,я пошел спать

    if(!text || !text.length) {
        throw Error('advkremove: No text was passed');
    }

}

function tokenize(text) {
    text = text.replace(/\r|\n/g, '');
    let tokens = Az.Tokens(text).done(['WORD']);

    let linksQuantity = Az.Tokens(text).done(['LINK']).length;

    return tokens
        .map(t => t.toString().toLowerCase())
        .filter(token => !(stopWords.indexOf(token) > -1))
        .map(token => {
            stemmer.setCurrent(token);
            stemmer.stem();

            return stemmer.getCurrent();
        })
        .concat(getLinksArray(linksQuantity));
}

function getLinksArray(size) {
    const mockWord = 'naivebayeslinkmockQiWsWaF';
    return Array(size).fill(mockWord);
}

function prepareBagForFile(bag) {
    let str = '';
    bag.forEach(item => {
        str += `${item.word}=${item.count},\n`;
    });

    return str;
}

function getTotalWordsCount(bagOfWords) {
    let count = 0;
    bagOfWords.forEach((item) => count += +item.count);

    return count;
}

// function removeDuplicates(array) {
//     return array.filter((item, pos, arr) => arr.indexOf(item) === pos);
// }