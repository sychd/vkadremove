var natural = require('natural'),
    porterStemmer = natural.PorterStemmerRu,
    classifier = new natural.BayesClassifier(porterStemmer);

const ads = require('./AdPostsData').adPostsData.split('~~~');
const LemSolaris = require('./GoodPostsData').LemSolaris;
const naiveBayes = require('./naiveBayes').naiveBayes;

ads.slice(0,100).forEach(text => classifier.addDocument(text, 'neg'));
classifier.addDocument(LemSolaris.substr(0,1550), 'pos');
classifier.train();

//ads.slice(0,200).forEach(text => !naiveBayes.isNegative(text) && console.log(text.substr(0,15)));
const text = 'я приехал домой, а ты уже дома?  на улице светает, но машины уже мчатся стремглав на восток';
console.log("> ", classifier.getClassifications(text));