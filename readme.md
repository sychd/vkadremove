It's a repo of VK advertise remover, that is is targeted to remove advertised posts in vk news feed.

First time usage
npm i

To build application:
npm run build

To run hot reload code (not dev-server) - recreates bundles on fly:
npm start

To add app to chrome extensions (dev):
1. build the app as mentioned above
2. add vkadremove/dist as unpacked extension to chrome://extensions